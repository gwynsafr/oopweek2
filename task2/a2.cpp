/* 2 - for, while
a. Напишите программу, которая на вход получает высоту пирамиды и выводит ее псевдосимволами в консоль. Пример: 
  #  #
 ##  ##
###  ###
*/
#include <iostream>
#include <string>
using namespace std;

int main() {
    cout << "Высота пирамиды ";
    int pir_len;
    cin >> pir_len;
    for (int spaces = pir_len-1, sybls = 1; sybls <= pir_len; spaces--, sybls++) {
        cout << string(spaces, ' ') + string(sybls, '#') + string(2, ' ') + string(sybls, '#') << endl;
    }
}
