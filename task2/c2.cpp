/* 2 - for, while
c. Напишите программу, реализующую алгоритм определения простых чисел (решето Эратосфена). 
*/
#include <iostream>
using namespace std;

int main() {
    // Задаём значения и массив чисел для просеивания
    int number_of_checked_values = 74;
    int *data_array = new int[number_of_checked_values + 1];
    for (int i = 0; i <= number_of_checked_values; i++)
    data_array[i] = i;
    
    // Обнуляем все сложные числа
    for (int i = 2; i * i <= number_of_checked_values; i++) {
        if (data_array[i])
            for (int j = i*i; j <= number_of_checked_values; j += i)
                data_array[j] = 0; 
    }
    
    for (int i = 2; i < number_of_checked_values; i++)
    {
        if (data_array[i]) {
            cout << data_array[i] << ", ";
        }
    }
    // Уборка за собой
    delete[] data_array;
}
