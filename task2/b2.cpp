/* 2 - for, while
b. Напишите программу, которая для указанного числа найдет значение квадратного корня, используя итерационную формулу Герона. Используйте цикл while.
*/
#include <iostream>
using namespace std;

int main() {
    double value, accuracy, xn, xn1 = 1;
    cout << "Вычисление квадратного корня\n";
    cout << "Число: ";
    cin >> value;
    cout << "Точность: ";
    cin >> accuracy;
    while (abs(xn1-xn) >= accuracy) {
        xn = xn1;
        xn1 = (xn + value/xn)/2;
    }
    cout <<"sqrt(" << value << ") = " << xn1 << endl;
    return 0;
}
