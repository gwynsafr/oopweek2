/* 1 - if, switch
c. Напишите программу которая решит за Вас, хотите ли вы играть в бадминтон :)
Известно, что вы предпочитаете играть только по воскресеньям, при этом должна быть хорошая погода ☀ (температура (жарко/тепло/холодно), осадки (ясно, облачно, дождь, снег, град), ветер (есть/нет), влажность (высокая/низкая)). На выходе, программа должна напечатать Да/Нет. 
*/
# include <iostream>
using namespace std;

int main() {
    // Ввод информации для определения результата
    int dayoftheweek;
    int temperature;
    int rain;
    int wind;
    int moisture;
    int final_score = 0;
    cout << "Введите информацию для определения того, хотите ли вы играть в бадминтон. При вводе указывайте номер предложенного варианта." << endl;
    cout << "Укажите следущие критерии:" << endl << "День недели (1-7): ";
    cin >> dayoftheweek;
    cout << "Температура (жарко, тепло, холодно): ";
    cin >> temperature;
    cout << "Осадки (ясно, облачно, дождь, снег, град): ";
    cin >> rain;
    cout << "Ветер (есть/нет): ";
    cin >> wind;
    cout << "Влажность (высокая/низкая): ";
    cin >> moisture;

    // Определение ответа
    if (dayoftheweek == 7) {
        final_score += 4;
    }

    // В таком случае лучше использовать map. Однострочные кейсы глаза мозолят
    switch (temperature) {
        case 1:
            final_score += 1;
            break;
        case 2:
            final_score += 2;
            break;
        case 3:
            final_score -= 2;
            break;    
        default:
            cout << "Failed to get temperature" << endl;
            break;
    }
    switch (rain) {
        case 1:
            final_score += 1;
            break;    
        case 2:
            final_score += 2;
            break; 
        case 3:
            final_score += 1 ;
            break; 
        case 4:
            final_score -= 2;
            break; 
        case 5:
            final_score -= 2;
            break; 
        default:
            cout << "Failed to get rain" << endl;
            break;
    }

    if (wind == 1) {
        final_score += 1;
    } else {
        final_score -= 0;
    }

    if (moisture == 1) {
        final_score -= 3;
    } else {
        final_score += 2;
    }
    
    // Вывод результата
    if (final_score >= 6) {
        cout << "Иди играй!";
    } else {
        cout << "Сиди дома...";
    }
}
