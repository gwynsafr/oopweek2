#include <iostream>
/* 1 - if, switch yeap
a. Напишите программу, которая принимает на вход целое число - возраст и выводит его с припиской год/лет и правильно склоняет (например - 21 год/22 года, 18 лет и т.д.) 
*/
using namespace std;

int main() {
    int age;
    cout << "Укажите ваш возраст: ";
    cin >> age;
    
    if (age >= 10 and age <= 20) {
        cout << age << " лет";
    }
        
    else {
        int last_digit = age % 10;
        switch(last_digit) {
            case 0:
                cout << age << " лет";
                break;
            
            case 1:
                cout << age << " год";
                break;
            
            case 2:
            case 3:
            case 4:
                cout << age << " года";
                break;
            
            default:
                cout << age << " лет";
                break;
    }
    }

}
