#include <iostream>
/* 1 - if, switch
b. Напишите программу, которая принимает на вход целое число - номер месяца и выводит количество дней в нем с учетом високостного года (расчитывать, а не вводить вопрос с клавиатуры).
*/
using namespace std;

int main() {
    /* Как расчитать висококосный год по номеру месяцы лол? 
    Типо, водишь 53 месяц и он считает?
    */
    int month_num;
    cout << "Введите номер месяца: ";
    cin >> month_num;
    switch (month_num) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            cout << "31 день" << endl;
            break;
        case 2:
            /* Проверка на висококосный год. Если это четвёрный год, то год считается висококосным. Не знаю как это сделать без чтения даты или ввода текущего года
            */
            if (month_num / 10 % 4 == 0) {
                cout << "29 дней" << endl;
                break;
            } else {
                cout << "28 дней" << endl;
                break;
            }
        case 4:
            cout << "30 дней" << endl;
            break;
        case 6:
            cout << "30 дней" << endl;
            break;
        case 9:
            cout << "30 дней" << endl;
            break;
        case 11:
            cout << "30 дней" << endl;
            break;
    }
}
