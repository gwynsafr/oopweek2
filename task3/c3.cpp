/*
3. Задачи на массивы
*Используйте простые массивы типа double array[]. Не используйте Containers library(vector, list и т.п.).
c. Напишите программу, которая на вход получает набор чисел в виде массива. Необходимо найти медиану этого набора чисел.
*/
#include <iostream>
#include <bits/stdc++.h>
using namespace std;

double find_median(double *array, int array_length) {
    double median;

    if (array_length % 2 == 0) {
        median = (array[array_length/2] + array[array_length/2 - 1]) / 2;
    }
    else {
        median = array[(array_length - 1) / 2];
    }
    return median;
}

int main() {
    int array_length;
    cout << "Укажите размер массива: ";
    cin >> array_length;
    cout << "Заполните массив." << endl;
    double *array = new double[array_length];
    	for (int i = 0; i < array_length; i++) {
            cin >> array[i];
	}
    sort(array, array + array_length, greater<int>());
    cout << find_median(array, array_length);
    return 0;
}
