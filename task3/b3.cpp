/*
3. Задачи на массивы
*Используйте простые массивы типа int data_array[]. Не используйте Containers library(vector, list и т.п.).
b. Напишите программу, реализующую алгоритм сортировки mergesort. Сортировку оформить отдельным методом.
*/
#include <iostream>
using namespace std;
 
void merge(double array[], int left, int mid, int right)
{
    int left_array_length = mid - left + 1;
    int right_array_length = right - mid;
 
    int *left_array = new int[left_array_length];
    int *right_array = new int[right_array_length];
 
    for (int i = 0; i < left_array_length; i++)
        left_array[i] = array[left + i];
    for (int j = 0; j < right_array_length; j++)
        right_array[j] = array[mid + 1 + j];
 
    int left_arr_index = 0;
    int right_arr_index = 0;
    int merged_arr_index = left;
 
    while (left_arr_index < left_array_length && right_arr_index < right_array_length) {
        if (left_array[left_arr_index] <= right_array[right_arr_index]) {
            array[merged_arr_index] = left_array[left_arr_index];
            left_arr_index++;
        }
        else {
            array[merged_arr_index] = right_array[right_arr_index];
            right_arr_index++;
        }
        merged_arr_index++;
    }
    while (left_arr_index < left_array_length) {
        array[merged_arr_index] = left_array[left_arr_index];
        left_arr_index++;
        merged_arr_index++;
    }
    while (right_arr_index < right_array_length) {
        array[merged_arr_index] = right_array[right_arr_index];
        right_arr_index++;
        merged_arr_index++;
    }
    delete[] left_array;
    delete[] right_array;
}
 
void mergesort(double array[], int begin, int end)
{
    if (begin >= end)
        return;
 
    int mid = begin + (end - begin) / 2;
    mergesort(array, begin, mid);
    mergesort(array, mid + 1, end);
    merge(array, begin, mid, end);
}
 
void printarray(double array[], int array_length)
{
    for (int i = 0; i < array_length; i++)
        cout << array[i] << " ";
}
 
int main() {
    int array_length;
    cout << "Укажите размер массива: ";
    cin >> array_length;
    cout << "Заполните массив." << endl;
    double *array = new double[array_length];
    	for (int i = 0; i < array_length; i++) {
            cin >> array[i];
	}
    mergesort(array, 0, array_length - 1);
 
    printarray(array, array_length);

    delete [] array;
    
    return 0;
}
