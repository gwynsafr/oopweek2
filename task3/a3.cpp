/*
3. Задачи на массивы
*Используйте простые массивы типа double data_array[]. Не используйте Containers library(vector, list и т.п.).
a. Напишите программу, которая на вход получает набор чисел в виде массива. Необходимо вывести количество чисел, равных максимальному.
*/

#include <iostream>
using namespace std;

int main() {
    cout << "Введите предполагаемое кол-во вводимых чисел: ";
    int array_length;
    cin >> array_length;
    
    double *data_array = new double[array_length];
    for (int i=0; i+1 <= array_length; i++) {
        cin >> data_array[i];
    }
    
    double max_value = data_array[0];
    int max_value_count = 1;
    
    for (int i=1; i+1 <= array_length; i++) {
        if (data_array[i] > max_value) {
            max_value = data_array[i];
            max_value_count = 1;
        }
        else if (data_array[i] == max_value) {
            max_value_count += 1;
        }
    }

    cout << "Максимальное значение (" << max_value << ") повторилось " << max_value_count << " раз в массиве!" << endl;
    
    delete [] data_array;
}
